<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Route::get('/bibek/{id}/{name}', function ($id, $name) {
//     return "hi bibek lama " . $id . $name;
// });
// Route::get('/lama/bibek/nepal', array('as' => 'admin.home', function () {
//     $url = route('admin.home');
//     return "this is url" . $url;
// }));
// Route::get('/lama', function () {
//     $url = route('admin.home');
//     return "this is another route" . $url;
// });
// Route::get('/contact/{id}/{name}/{password}', 'PostController@contact');
// Route::resource('post', "PostController");
// Route::get('/insert',function(){
//     DB::insert('insert into posts(title,content,addedColumn) values (?,?,?)',['bibek','lama','baikhu']);
// });
// Route::get('/delete',function (){
//    $deleted=DB::delete('delete from posts where id=?',[1]);
//    return $deleted;
// });
// Route::get('/select',function (){
//     $selected=DB::select('select * from posts where id=?',[2]);
//     return $selected;
// });
// Route::get('update',function(){
//    $updated =  DB::update('update posts set title="pooja" where id=?',[2]);
//    return $updated;
  
// });
// ELOQUENT
use App\Post;
// Route::get('/all',function(){
//     $post = Post::find(2);
//     return $post;
// });
// Route::get('/insert',function(){
//     $post =Post::find(3);
//     $post->title='pujaaaaa';
//     $post->content='baccha';
//     $post->addedColumn='ramre';
//     $post->save();
// });
// Route::get('/insert',function(){
//     Post::create(['title'=>'ram','content'=>'handsome','addedColumn'=>'done']);
// });
// Route::get('/update',function(){
//     Post::where('id',2)->update(['title'=>'bibek']);
// });
// Route::get("/delete",function(){
//     Post::destroy(2);
// });
// Route::get('/softdelete',function(){
//     Post::find(4)->delete();
// });
// Route::get('/readsoftdelete',function(){
//     $post = Post::onlyTrashed()->get();
//     return $post;
// });
// Route::get('/restore',function(){
//     $post = Post::onlyTrashed()->restore();
//     return $post;
// });
// Route::get('forcedelete',function(){
//     Post::onlyTrashed()->forceDelete();
// });
Use App\User;
use Illuminate\Routing\Router;

// Route::get('/user',function(){
//     return User::find(1)->post;
// });
// Route::get('/manypost',function(){
//     $user = User::find(1);
//     foreach($user->posts as $post){
//             echo $post->title;
//     }
// });
// Route::get('/manytomany',function(){
//     $user = User::find(3);
//    foreach($user->roles as $role){
//        return $role->name;
//    }
// });
// Route::get('/pivot',function(){
//     $user = User::find(1);
//     foreach($user->roles as $role){
//         return $role->pivot->created_at;
//     }
// });
// use App\Country;
// Route::get('hasmanythrough',function(){
//         $country = Country::find(1);
//         foreach($country->posts as $post){
//             echo $post->title;
//         }
// });

// Route::get('/user/photos',function(){
//     $user = User::find(1);
//     foreach($user->photos as $photo){
//         return $photo;
//     }
// });

// Route::get('/post/photos',function(){
//     $posts = Post::find(1);
//     foreach($posts->photos as $photo){
//         return $photo;
//     }
// });
use App\Photo;
// Route::get('owner',function(){
//     $photo = Photo::find(1);
//     return $photo->imageable;
// });

//polymorphic many to many

// Route::get('/tags',function(){
//     $post = Post::find(1);
//     foreach($post->tags as $tag){
//         echo $tag->name;
//     }
// });
// use App\Tag;
// Route::get('tag',function(){
//     $tag = Tag::find(2);
//     foreach($tag->posts as $post){
//         return $post;
//     }
// });

//crud applications

use Carbon\Carbon;
Route::group(['middleware'=>'web'],function(){
    Route::resource('/posts','PostController');
    Route::get('/dates',function(){
        $date = new DateTime();
        echo $date->format('m-d-Y');
        echo '<br>';
        echo Carbon::now()->addDay(10)->diffForHumans();
    });
    Route::get('/name',function(){
        $user = User::find(1);
        return $user->name;
    });
    Route::get('/setname',function(){
        $user = User::find(1);
        $user->name = 'lamadai';
        $user->save();
    });
});
