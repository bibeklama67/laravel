<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
use App\User;
use App\Role;

Route::get('/create',function(){
    $user = User::find(1);
    $role = new Role(['name'=>'admin']);
    $user->roles()->save($role);
    return 'craeted';
});

Route::get('/read',function(){
    $user = User::find(1);
    // return $user->roles;
    foreach($user->roles as $role){
        dd($role);
    }
});

Route::get('/update',function(){
    $user = User::find(1);
    if($user->has('roles')){
        foreach($user->roles as $role){
            if($role->name == 'admin'){
                $role->name = 'updated admin';
                $role->save();
            }
        }
    }
});

Route::get('/delete',function(){
    $user = User::find(1);
    foreach($user->roles as $role){
        $role->whereId(2)->delete();
    }
});
Route::get('/attach',function(){
 $user = User::find(1);
 $user -> roles()->attach(3);
});

Route::get('/detach',function(){
    $user = User::find(1);
    $user -> roles()->detach();
});

Route::get('/sync',function(){
    $user = User::find(1);
    $user->roles()->sync([2]);
});