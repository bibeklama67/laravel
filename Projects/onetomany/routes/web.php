<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
use App\User;
use App\Post;
Route::get('/insert',function(){
    $user = User::find(1);
    $post  =  new Post();
    $post->title = 'post';
    $post->body = 'the body of the post hai guys';
    $user->posts()->save($post);
    return 'created post';
});

Route::get('/update',function(){
    $user = User::find(1);
    $user->posts()->where('id',2)->update(['title'=>'bibek']);
});

Route::get('/read',function(){
    $user = User::find(1);
    // return $user->posts;
    foreach($user->posts as $post){
        echo $post->body."<br/>";
    }
});

Route::get('/delete',function(){
    $user = User::find(1);
    $user->posts()->delete();
    return 'deleted';
});