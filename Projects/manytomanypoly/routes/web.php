<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
use App\Post;
use App\Video;
use App\Tag;

Route::get('create',function(){
    $post = Post::create(['name'=>'bibek post ']);
    $tag1 = Tag::find(1);
    $post->tags()->save($tag1);
    $video = Video::create(['name'=>'video ho hai']);
    $tag2 = Tag::find(2);
    $video->tags()->save($tag2);
});

Route::get('/read',function(){
    $post = Post::find(4);
    return $post;
    foreach($post->tags as $tag){
        echo $tag;
    }
});

Route::get('/update',function(){
    $post = Post::find(1);
    foreach($post->tags as $tag){
        return $tag->whereName('bibek')->update(['name'=>'puja']);
    }
});

Route::get('/delete',function(){
    $post = Post::find(1);
    foreach($post->tags as $tag){
        $tag->whereId(1)->delete();
    }
});