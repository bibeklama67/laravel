<?php

use App\Adress;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
Route::get('/', function () {
    return view('welcome');
});

Route::get('/insert',function(){
    $user = User::findOrFail(1);
    $adress = new Adress(['name'=>'Kathmandu']);
    $user->adress()->save($adress);

});
Route::get('/update',function(){
    $adress = Adress::where('user_id','=',1)->first();

    // $adress = Adress::where('user_id',1);
    // $adress = Adress::whereUserId(1);
    $adress->name = 'updated kathmnadu';
    $adress->save();
});

Route::get('/read',function(){
    $user = User::find(1);
    return $user->adress->name;
});
Route::get('/delete',function(){
    $user = User::find(1);
    $user->adress()->delete();
    return 'done';
});