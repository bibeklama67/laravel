@extends('layout.app')
@section('content')
<h1>Edit Page</h1>
{!! Form::model($post,['method'=>'PATCH','action'=>['PostController@update',$post->id]]) !!}
{{csrf_field() }}
  <div class="form-group">
      {!! Form::label('title','Title') !!}
      {!! Form::text('title',null,['class'=>'form-control','value'=>$post->title]) !!}
  </div>
  <div class="form-group">
   
    {!! Form::submit('Update',null,['class'=>'btn btn-primary']) !!}
</div>
{!! Form::close() !!}
{!! Form::model($post,['method'=>'DELETE','action'=>['PostController@destroy',$post->id]]) !!}
{{csrf_field() }}
 
  <div class="form-group">
   
    {!! Form::submit('Delete',null,['class'=>'btn btn-danger']) !!}
</div>
{!! Form::close() !!}
@yield('footer')