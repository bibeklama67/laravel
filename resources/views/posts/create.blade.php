@extends('layout.app')
@section('content')
{!! Form::open(['method'=>'post','action'=>'PostController@store','files'=>true]) !!}
  <div class="form-group">
      {!! Form::label('title','Title') !!}
      {!! Form::text('title',null,['class'=>'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('file','File Chooser') !!}
    {!! Form::file('file',null,['class'=>'form-control']) !!}
</div>
  <div class="form-group">
   
    {!! Form::submit('Create',null,['class'=>'btn btn-primary']) !!}
</div>
{!! Form::close() !!}
@if(count($errors) > 0)
  <div>
    <ul>
      @foreach($errors->all() as $error)
        {{$error}}
      @endforeach
    </ul>
  </div>
@endif
@yield('footer')