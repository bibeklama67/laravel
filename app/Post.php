<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $directory = "/images/";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=[
        'title',
        'content',
        'addedColumn',
        'path'
    ];
    public function photos(){
        return $this->morphMany('App\Photo','imageable');
    }
    public function tags(){
        return $this->morphToMany('App\Tag','taggable');
    }
    public static function scopeBibek($query){
       return $query->orderBy('id','desc')->get();
    }
    public function getPathAttribute($value){
        return $this->directory . $value;
    }
}
